package org.vindkaldr.account.api

interface AccountBoundary {
    fun createAccount(request: CreateAccountRequest): CreateAccountResponse
    fun readAccount(request: ReadAccountRequest): ReadAccountResponse
    fun deleteAccount(request: DeleteRequest): DeleteAccountResponse
}
