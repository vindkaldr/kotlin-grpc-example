package org.vindkaldr.account.api

interface AccountBoundaryFactory {
    fun create(): AccountBoundary
    fun create(correlationId: String, apiToken: String): AccountBoundary
}
