package org.vindkaldr.account.api

data class CreateAccountRequest(val email: String)
