package org.vindkaldr.account.api

data class CreateAccountResponse(val apiToken: String)
