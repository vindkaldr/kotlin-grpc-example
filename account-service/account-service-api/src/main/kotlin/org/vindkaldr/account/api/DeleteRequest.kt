package org.vindkaldr.account.api

data class DeleteRequest(val apiToken: String)
