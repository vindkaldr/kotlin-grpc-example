package org.vindkaldr.account.api

data class ReadAccountRequest(val correlationId: String, val apiToken: String)
