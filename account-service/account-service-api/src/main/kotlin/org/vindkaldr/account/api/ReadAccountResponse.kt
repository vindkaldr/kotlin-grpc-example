package org.vindkaldr.account.api

data class ReadAccountResponse(val email: String)
