package org.vindkaldr.account.client.api

sealed class Account
object UnregisteredAccount: Account()
class RegisteredAccount(val email: String, val apiToken: String): Account()
