package org.vindkaldr.account.client.api

interface AccountServiceClient {
    fun createAccount(email: String): RegisteredAccount
    fun readAccount(correlationId: String, apiToken: String): RegisteredAccount
    fun deleteAccount(apiToken: String)
}
