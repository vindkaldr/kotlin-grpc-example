package org.vindkaldr.account.client

import org.vindkaldr.account.client.api.AccountServiceClient

object AccountServiceClientFactory {
    fun create(): AccountServiceClient {
        return DefaultAccountServiceClient(GrpcAccountBoundary())
    }
}
