package org.vindkaldr.account.client

import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.api.CreateAccountRequest
import org.vindkaldr.account.api.DeleteRequest
import org.vindkaldr.account.api.ReadAccountRequest
import org.vindkaldr.account.client.api.AccountServiceClient
import org.vindkaldr.account.client.api.RegisteredAccount

class DefaultAccountServiceClient(
    private val accountBoundary: AccountBoundary
): AccountServiceClient {
    override fun createAccount(email: String): RegisteredAccount {
        val request = CreateAccountRequest(email)
        val response = accountBoundary.createAccount(request)
        return RegisteredAccount(email, response.apiToken)
    }

    override fun readAccount(correlationId: String, apiToken: String): RegisteredAccount {
        val request = ReadAccountRequest(correlationId, apiToken)
        val response = accountBoundary.readAccount(request)
        return RegisteredAccount(response.email, apiToken)
    }

    override fun deleteAccount(apiToken: String) {
        accountBoundary.deleteAccount(DeleteRequest(apiToken))
    }
}
