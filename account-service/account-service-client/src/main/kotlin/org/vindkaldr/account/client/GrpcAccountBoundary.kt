package org.vindkaldr.account.client

import account.AccountServiceGrpc
import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.api.CreateAccountRequest
import org.vindkaldr.account.api.CreateAccountResponse
import org.vindkaldr.account.api.DeleteAccountResponse
import org.vindkaldr.account.api.DeleteRequest
import org.vindkaldr.account.api.ReadAccountRequest
import org.vindkaldr.account.api.ReadAccountResponse
import account.AccountServiceOuterClass.CreateAccountRequest as GrpcCreateAccountRequest
import account.AccountServiceOuterClass.DeleteAccountRequest as GrpcDeleteAccountRequest
import account.AccountServiceOuterClass.ReadAccountRequest as GrpcReadAccountRequest

class GrpcAccountBoundary : AccountBoundary {
    private val accountService = AccountServiceGrpc.newBlockingStub(
        ManagedChannelBuilder.forAddress("gateway", 80)
            .usePlaintext()
            .build()
    )

    override fun createAccount(request: CreateAccountRequest): CreateAccountResponse = catchError {
        val grpcRequest = GrpcCreateAccountRequest.newBuilder()
            .setEmail(request.email)
            .build()
        val grpcResponse = accountService.createAccount(grpcRequest)

        return@catchError CreateAccountResponse(grpcResponse.apiToken)
    }

    override fun readAccount(request: ReadAccountRequest): ReadAccountResponse = catchError {
        val grpcRequest = GrpcReadAccountRequest.newBuilder()
            .setCorrelationId(request.correlationId)
            .setApiToken(request.apiToken)
            .build()
        val grpcResponse = accountService.readAccount(grpcRequest)

        return@catchError ReadAccountResponse(grpcResponse.email)
    }

    override fun deleteAccount(request: DeleteRequest): DeleteAccountResponse = catchError {
        val grpcRequest = GrpcDeleteAccountRequest.newBuilder()
            .setApiToken(request.apiToken)
            .build()
        accountService.deleteAccount(grpcRequest)

        return@catchError DeleteAccountResponse
    }

    private fun <T> catchError(block: () -> T): T {
        try {
            return block()
        }
        catch (e: StatusRuntimeException) {
            throwException(e.status)
        }
    }

    private fun throwException(status: Status): Nothing {
        if (status == Status.INVALID_ARGUMENT) {
            throw IllegalArgumentException()
        }
        else if (status == Status.UNAUTHENTICATED) {
            throw SecurityException()
        }
        throw Throwable()
    }
}
