package org.vindkaldr.grpc

import account.AccountServiceGrpc
import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.grpc.stub.StreamObserver
import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.api.CreateAccountRequest
import org.vindkaldr.account.api.CreateAccountResponse
import org.vindkaldr.account.api.DeleteAccountResponse
import org.vindkaldr.account.api.DeleteRequest
import org.vindkaldr.account.api.ReadAccountRequest
import org.vindkaldr.account.api.ReadAccountResponse
import org.vindkaldr.grpc.module.GrpcModule
import account.AccountServiceOuterClass.CreateAccountRequest as GrpcCreateAccountRequest
import account.AccountServiceOuterClass.CreateAccountResponse as GrpcCreateAccountResponse
import account.AccountServiceOuterClass.DeleteAccountRequest as GrpcDeleteAccountRequest
import account.AccountServiceOuterClass.DeleteAccountResponse as GrpcDeleteAccountResponse
import account.AccountServiceOuterClass.ReadAccountRequest as GrpcReadAccountRequest
import account.AccountServiceOuterClass.ReadAccountResponse as GrpcReadAccountResponse

class GrpcAccountService(
    private val accountBoundary : AccountBoundary
) : AccountServiceGrpc.AccountServiceImplBase() {
    constructor(grpcModule: GrpcModule) : this(grpcModule.accountBoundary)

    override fun createAccount(
        grpcRequest: GrpcCreateAccountRequest,
        grpcResponseObserver: StreamObserver<GrpcCreateAccountResponse>
    ) = catchError(grpcResponseObserver) {
        val response = accountBoundary.createAccount(grpcRequest.toRequest())

        grpcResponseObserver.onNext(response.toGrpcResponse())
        grpcResponseObserver.onCompleted()
    }

    override fun readAccount(
        grpcRequest: GrpcReadAccountRequest,
        grpcResponseObserver: StreamObserver<GrpcReadAccountResponse>
    ) = catchError(grpcResponseObserver) {
        val response = accountBoundary.readAccount(grpcRequest.toRequest())

        grpcResponseObserver.onNext(response.toGrpcResponse())
        grpcResponseObserver.onCompleted()
    }

    override fun deleteAccount(
        grpcRequest: GrpcDeleteAccountRequest,
        grpcResponseObserver: StreamObserver<GrpcDeleteAccountResponse>
    ) = catchError(grpcResponseObserver) {
        val response = accountBoundary.deleteAccount(grpcRequest.toRequest())

        grpcResponseObserver.onNext(response.toGrpcResponse())
        grpcResponseObserver.onCompleted()
    }
}

private fun catchError(streamObserver: StreamObserver<*>, block: () -> Unit) {
    try {
        block()
    }
    catch (t: Throwable) {
        println(t)
        streamObserver.onError(StatusRuntimeException(mapToStatus(t)))
    }
}

private fun mapToStatus(t: Throwable): Status {
    return when (t) {
        is IllegalArgumentException -> Status.INVALID_ARGUMENT
        is SecurityException -> Status.UNAUTHENTICATED
        else -> Status.INTERNAL
    }
}

private fun GrpcCreateAccountRequest.toRequest(): CreateAccountRequest {
    return CreateAccountRequest(email)
}

private fun CreateAccountResponse.toGrpcResponse(): GrpcCreateAccountResponse {
    return GrpcCreateAccountResponse.newBuilder()
        .setApiToken(apiToken)
        .build()
}

private fun GrpcReadAccountRequest.toRequest(): ReadAccountRequest {
    return ReadAccountRequest(correlationId, apiToken)
}

private fun ReadAccountResponse.toGrpcResponse(): GrpcReadAccountResponse {
    return GrpcReadAccountResponse.newBuilder()
        .setEmail(email)
        .build()
}

private fun GrpcDeleteAccountRequest.toRequest(): DeleteRequest {
    return DeleteRequest(apiToken)
}

private fun DeleteAccountResponse.toGrpcResponse(): GrpcDeleteAccountResponse {
    return GrpcDeleteAccountResponse.newBuilder().build()
}
