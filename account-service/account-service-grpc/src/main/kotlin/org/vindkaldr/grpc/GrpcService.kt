package org.vindkaldr.grpc

import io.grpc.Server
import io.grpc.ServerBuilder
import org.vindkaldr.grpc.api.Service
import org.vindkaldr.grpc.module.GrpcModule

const val DEFAULT_PORT = 50051

class GrpcService constructor(
    private val grpcAccountService: GrpcAccountService
) : Service {
    constructor(grpcModule: GrpcModule) : this(grpcModule.grpcAccountService)

    private lateinit var server: Server

    override fun start() {
        server = ServerBuilder.forPort(getPort())
            .addService(grpcAccountService)
            .build()
            .start()
    }

    private fun getPort(): Int {
        val portFromEnvironment = System.getenv("PORT")
        return when {
            portFromEnvironment.isNotBlank() -> portFromEnvironment.toInt()
            else -> DEFAULT_PORT
        }
    }

    override fun awaitTermination() {
        server.awaitTermination()
    }
}
