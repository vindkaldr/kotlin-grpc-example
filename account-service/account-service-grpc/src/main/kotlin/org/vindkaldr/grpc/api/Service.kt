package org.vindkaldr.grpc.api

interface Service {
    fun start()
    fun awaitTermination()
}
