package org.vindkaldr.grpc.module

import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.grpc.GrpcAccountService
import org.vindkaldr.grpc.GrpcService

interface GrpcModule {
    val service: GrpcService
    val grpcAccountService: GrpcAccountService
    val accountBoundary: AccountBoundary
}
