package org.vindkaldr.account.main

fun main() {
    val service = MainModule().service
    service.start()
    service.awaitTermination()
}
