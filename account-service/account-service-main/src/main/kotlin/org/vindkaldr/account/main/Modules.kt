package org.vindkaldr.account.main

import org.vindkaldr.account.CreateAccountService
import org.vindkaldr.account.DefaultAccountBoundary
import org.vindkaldr.account.DeleteAccountService
import org.vindkaldr.account.ReadAccountService
import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.api.CreateAccountRequest
import org.vindkaldr.account.api.CreateAccountResponse
import org.vindkaldr.account.api.DeleteAccountResponse
import org.vindkaldr.account.api.DeleteRequest
import org.vindkaldr.account.api.ReadAccountRequest
import org.vindkaldr.account.api.ReadAccountResponse
import org.vindkaldr.account.client.api.Account
import org.vindkaldr.account.module.AccountModule
import org.vindkaldr.account.module.PluginsModule
import org.vindkaldr.account.repository.InMemoryAccountRepository
import org.vindkaldr.grpc.GrpcAccountService
import org.vindkaldr.grpc.GrpcService
import org.vindkaldr.grpc.module.GrpcModule

class MainModule {
    private val accountModuleFactory: AccountModuleFactory =
        { correlationId: String, apiToken: String ->
            DefaultAccountModule(correlationId, apiToken, pluginsModule) }

    private val grpcModule by lazy { DefaultGrpcModule(accountModuleFactory) }
    private val pluginsModule by lazy { DefaultPluginsModule() }

    val service by lazy { grpcModule.service }
}

private class DefaultAccountModule(
    override val correlationId: String,
    private val apiToken: String,
    private val pluginsModule: PluginsModule
) : AccountModule {
    override val accountBoundary by lazy { DefaultAccountBoundary(this) }
    override val createAccountService by lazy { CreateAccountService(pluginsModule) }
    override val readAccountService by lazy { ReadAccountService(this) }
    override val deleteAccountService by lazy { DeleteAccountService(this, pluginsModule) }
    override val account: Account by lazy { pluginsModule.accountRepository.get(apiToken) }
}

private typealias AccountModuleFactory = (correlationId: String, apiToken: String) -> AccountModule

private class DefaultGrpcModule(
    private val accountModuleFactory: AccountModuleFactory
) : GrpcModule {
    override val service: GrpcService by lazy { GrpcService(this) }
    override val grpcAccountService by lazy { GrpcAccountService(this) }
    override val accountBoundary by lazy {
        object : AccountBoundary {
            override fun createAccount(request: CreateAccountRequest): CreateAccountResponse {
                val accountModule = accountModuleFactory("", "")
                val accountBoundary = accountModule.accountBoundary
                return accountBoundary.createAccount(request)
            }

            override fun readAccount(request: ReadAccountRequest): ReadAccountResponse {
                val accountModule = accountModuleFactory(request.correlationId, request.apiToken)
                val accountBoundary = accountModule.accountBoundary
                return accountBoundary.readAccount(request)
            }

            override fun deleteAccount(request: DeleteRequest): DeleteAccountResponse {
                val accountModule = accountModuleFactory("", request.apiToken)
                val accountBoundary = accountModule.accountBoundary
                return accountBoundary.deleteAccount(request)
            }
        }
    }
}

private class DefaultPluginsModule : PluginsModule {
    override val accountRepository by lazy { InMemoryAccountRepository() }
}
