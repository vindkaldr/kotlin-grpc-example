package org.vindkaldr.account

import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.account.module.PluginsModule
import org.vindkaldr.account.repository.api.AccountRepository
import java.util.UUID

class CreateAccountService(private val accountRepository: AccountRepository) {
    constructor(pluginsModule: PluginsModule) : this(pluginsModule.accountRepository)

    fun create(email: String): RegisteredAccount {
        val account = RegisteredAccount(email, createApiToken())
        accountRepository.add(account)
        println("Created account: ${account.apiToken}")
        return account
    }

    private fun createApiToken() = UUID.randomUUID().toString()
}
