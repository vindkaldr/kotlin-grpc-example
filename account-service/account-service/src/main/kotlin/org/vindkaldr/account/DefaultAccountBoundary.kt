package org.vindkaldr.account

import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.api.CreateAccountRequest
import org.vindkaldr.account.api.CreateAccountResponse
import org.vindkaldr.account.api.DeleteAccountResponse
import org.vindkaldr.account.api.DeleteRequest
import org.vindkaldr.account.api.ReadAccountRequest
import org.vindkaldr.account.api.ReadAccountResponse
import org.vindkaldr.account.module.AccountModule

class DefaultAccountBoundary(
    private val createAccountService: CreateAccountService,
    private val readAccountService: ReadAccountService,
    private val deleteAccountService: DeleteAccountService
) : AccountBoundary {
    constructor(accountModule: AccountModule) : this(
        accountModule.createAccountService,
        accountModule.readAccountService,
        accountModule.deleteAccountService
    )

    override fun createAccount(request: CreateAccountRequest): CreateAccountResponse {
        val account = createAccountService.create(request.email)
        return CreateAccountResponse(account.apiToken)
    }

    override fun readAccount(request: ReadAccountRequest): ReadAccountResponse {
        val account = readAccountService.read()
        return ReadAccountResponse(account.email)
    }

    override fun deleteAccount(request: DeleteRequest): DeleteAccountResponse {
        deleteAccountService.delete()
        return DeleteAccountResponse
    }
}
