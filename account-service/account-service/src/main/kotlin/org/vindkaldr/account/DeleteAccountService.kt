package org.vindkaldr.account

import org.vindkaldr.account.client.api.Account
import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.account.client.api.UnregisteredAccount
import org.vindkaldr.account.module.AccountModule
import org.vindkaldr.account.module.PluginsModule
import org.vindkaldr.account.repository.api.AccountRepository

class DeleteAccountService(
    private val account: Account,
    private val accountRepository: AccountRepository
) {
    constructor(accountModule: AccountModule, pluginsModule: PluginsModule) : this(
        accountModule.account,
        pluginsModule.accountRepository
    )

    fun delete() {
        when (account) {
            is UnregisteredAccount -> throw IllegalArgumentException()
            is RegisteredAccount -> {
                check(accountRepository.contains(account.apiToken))
                accountRepository.remove(account.apiToken)
                println("Deleted account: ${account.apiToken}")
            }
        }
    }
}
