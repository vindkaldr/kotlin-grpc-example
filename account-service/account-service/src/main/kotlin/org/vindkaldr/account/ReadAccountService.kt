package org.vindkaldr.account

import org.vindkaldr.account.client.api.Account
import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.account.client.api.UnregisteredAccount
import org.vindkaldr.account.module.AccountModule

class ReadAccountService(
    private val correlationId: String,
    private val account: Account
) {
    constructor(accountModule: AccountModule) : this(
        accountModule.correlationId,
        accountModule.account
    )

    fun read(): RegisteredAccount {
        return when (account) {
            is UnregisteredAccount -> {
                println("($correlationId) SecurityException")
                throw SecurityException()
            }
            is RegisteredAccount -> account
        }
    }
}
