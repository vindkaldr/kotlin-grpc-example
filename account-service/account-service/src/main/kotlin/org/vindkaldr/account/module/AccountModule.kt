package org.vindkaldr.account.module

import org.vindkaldr.account.CreateAccountService
import org.vindkaldr.account.DeleteAccountService
import org.vindkaldr.account.ReadAccountService
import org.vindkaldr.account.api.AccountBoundary
import org.vindkaldr.account.client.api.Account

interface AccountModule {
    val accountBoundary: AccountBoundary
    val createAccountService: CreateAccountService
    val readAccountService: ReadAccountService
    val deleteAccountService: DeleteAccountService
    val account: Account
    val correlationId: String
}
