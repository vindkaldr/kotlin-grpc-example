package org.vindkaldr.account.module

import org.vindkaldr.account.repository.api.AccountRepository

interface PluginsModule {
    val accountRepository: AccountRepository
}
