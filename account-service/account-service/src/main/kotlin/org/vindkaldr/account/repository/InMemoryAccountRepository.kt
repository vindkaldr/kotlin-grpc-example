package org.vindkaldr.account.repository

import org.vindkaldr.account.client.api.Account
import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.account.client.api.UnregisteredAccount
import org.vindkaldr.account.repository.api.AccountRepository

class InMemoryAccountRepository : AccountRepository {
    private val accounts = mutableMapOf<String, Account>()

    @Synchronized
    override fun add(account: RegisteredAccount) {
        accounts[account.apiToken] = account
    }

    @Synchronized
    override fun contains(apiToken: String) = accounts.contains(apiToken)

    @Synchronized
    override fun get(apiToken: String): Account {
        return accounts[apiToken] ?: UnregisteredAccount
    }

    @Synchronized
    override fun remove(apiToken: String) {
        accounts.remove(apiToken)
    }
}
