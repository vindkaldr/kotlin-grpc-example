package org.vindkaldr.account.repository.api

import org.vindkaldr.account.client.api.Account
import org.vindkaldr.account.client.api.RegisteredAccount

interface AccountRepository {
    fun add(account: RegisteredAccount)
    fun contains(apiToken: String): Boolean
    fun get(apiToken: String): Account
    fun remove(apiToken: String)
}
