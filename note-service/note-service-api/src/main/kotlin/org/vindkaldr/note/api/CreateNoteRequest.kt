package org.vindkaldr.note.api

data class CreateNoteRequest(val correlationId: String, val apiToken: String, val note: Note)
