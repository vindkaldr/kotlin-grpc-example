package org.vindkaldr.note.api

data class CreateNoteResponse(val note: Note)
