package org.vindkaldr.note.api

data class DeleteRequest(val apiToken: String, val nodeId: String)
