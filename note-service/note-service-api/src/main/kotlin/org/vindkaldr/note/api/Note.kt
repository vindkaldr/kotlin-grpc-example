package org.vindkaldr.note.api

data class Note(val id: String, val note: String)
