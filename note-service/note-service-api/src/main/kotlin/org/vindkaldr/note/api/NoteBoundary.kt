package org.vindkaldr.note.api

interface NoteBoundary {
    fun createNote(request: CreateNoteRequest): CreateNoteResponse
}
