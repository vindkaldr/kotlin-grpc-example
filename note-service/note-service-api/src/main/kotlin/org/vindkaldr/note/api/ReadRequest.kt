package org.vindkaldr.note.api

data class ReadRequest(val apiToken: String, val nodeId: String)
