package org.vindkaldr.note.api

data class ReadResponse(val note: Note)
