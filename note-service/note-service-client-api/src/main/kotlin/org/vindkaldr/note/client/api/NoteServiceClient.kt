package org.vindkaldr.note.client.api

import org.vindkaldr.note.api.Note

interface NoteServiceClient {
    fun createNote(correlationId: String, apiToken: String, note: Note): Note
    fun readNote(apiToken: String, id: String): Note
    fun deleteNote(apiToken: String, id: String): Note
}
