package org.vindkaldr.note.client

import org.vindkaldr.note.api.CreateNoteRequest
import org.vindkaldr.note.api.Note
import org.vindkaldr.note.api.NoteBoundary
import org.vindkaldr.note.client.api.NoteServiceClient

internal class DefaultNoteServiceClient(
    private val noteBoundary: NoteBoundary
) : NoteServiceClient {
    override fun createNote(correlationId: String, apiToken: String, note: Note): Note {
        val request = CreateNoteRequest(correlationId, apiToken, note)
        val response = noteBoundary.createNote(request)
        return response.note
    }

    override fun readNote(apiToken: String, id: String): Note {
        TODO("not implemented")
    }

    override fun deleteNote(apiToken: String, id: String): Note {
        TODO("not implemented")
    }
}
