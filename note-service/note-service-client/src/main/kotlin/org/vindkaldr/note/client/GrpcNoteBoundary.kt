package org.vindkaldr.note.client

import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import note.NoteServiceGrpc
import org.vindkaldr.note.api.CreateNoteRequest
import org.vindkaldr.note.api.CreateNoteResponse
import org.vindkaldr.note.api.Note
import org.vindkaldr.note.api.NoteBoundary
import note.NoteServiceOuterClass.CreateNoteRequest as GrpcCreateNoteRequest
import note.NoteServiceOuterClass.CreateNoteResponse as GrpcCreateNoteResponse

internal class GrpcNoteBoundary : NoteBoundary {
    private val grpcNoteService = NoteServiceGrpc.newBlockingStub(
        ManagedChannelBuilder.forAddress("gateway", 80)
            .usePlaintext()
            .build()
    )

    override fun createNote(request: CreateNoteRequest): CreateNoteResponse = catchError {
        val grpcRequest = request.toGrpcRequest()
        val grpcResponse = grpcNoteService.createNote(grpcRequest)
        return@catchError grpcResponse.toResponse()
    }

    private fun CreateNoteRequest.toGrpcRequest() =
        GrpcCreateNoteRequest.newBuilder()
            .setCorrelationId(correlationId)
            .setApiToken(apiToken)
            .setNote(GrpcCreateNoteRequest.newBuilder().noteBuilder
                .setId(note.id)
                .setNote(note.note)
                .build())
            .build()

    private fun GrpcCreateNoteResponse.toResponse() =
        CreateNoteResponse(Note(note.id, note.note))

    private fun <T> catchError(block: () -> T): T {
        try {
            return block()
        }
        catch (e: StatusRuntimeException) {
            throwException(e.status)
        }
    }

    private fun throwException(status: Status): Nothing {
        if (status == Status.INVALID_ARGUMENT) {
            throw IllegalArgumentException()
        }
        else if (status == Status.UNAUTHENTICATED) {
            throw SecurityException()
        }
        throw Throwable()
    }
}
