package org.vindkaldr.note.client

import org.vindkaldr.note.client.api.NoteServiceClient

object NoteServiceClientFactory {
    fun create(): NoteServiceClient {
        return DefaultNoteServiceClient(GrpcNoteBoundary())
    }
}
