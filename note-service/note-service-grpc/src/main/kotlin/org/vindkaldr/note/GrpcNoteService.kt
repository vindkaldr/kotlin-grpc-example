package org.vindkaldr.note

import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.grpc.stub.StreamObserver
import note.NoteServiceGrpc
import org.vindkaldr.note.api.CreateNoteRequest
import org.vindkaldr.note.api.CreateNoteResponse
import org.vindkaldr.note.api.Note
import org.vindkaldr.note.api.NoteBoundary
import org.vindkaldr.note.grpc.module.GrpcModule
import note.NoteServiceOuterClass.CreateNoteRequest as GrpcCreateNoteRequest
import note.NoteServiceOuterClass.CreateNoteResponse as GrpcCreateNoteResponse

class GrpcNoteService(
    private val noteBoundary: NoteBoundary
) : NoteServiceGrpc.NoteServiceImplBase() {
    constructor(grpcModule: GrpcModule) : this(grpcModule.noteBoundary)

    override fun createNote(
        grpcRequest: GrpcCreateNoteRequest,
        grpcResponseObserver: StreamObserver<GrpcCreateNoteResponse>
    ) = catchError(grpcResponseObserver) {
        val createResponse = noteBoundary.createNote(grpcRequest.toRequest())

        grpcResponseObserver.onNext(createResponse.toGrpcResponse())
        grpcResponseObserver.onCompleted()
    }

    private fun GrpcCreateNoteRequest.toRequest() =
        CreateNoteRequest(correlationId, apiToken, Note(note.id, note.note))

    private fun CreateNoteResponse.toGrpcResponse() =
        GrpcCreateNoteResponse.newBuilder()
            .setNote(GrpcCreateNoteResponse.newBuilder().noteBuilder.
                setNote(note.note)
                .build())
            .build()

    private fun catchError(streamObserver: StreamObserver<*>, block: () -> Unit) {
        try {
            block()
        }
        catch (t: Throwable) {
            println(t)
            streamObserver.onError(StatusRuntimeException(mapToStatus(t)))
        }
    }

    private fun mapToStatus(t: Throwable): Status {
        return when (t) {
            is IllegalArgumentException -> Status.INVALID_ARGUMENT
            is SecurityException -> Status.UNAUTHENTICATED
            else -> Status.INTERNAL
        }
    }
}
