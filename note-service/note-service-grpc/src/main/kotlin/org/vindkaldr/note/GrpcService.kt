package org.vindkaldr.note

import io.grpc.Server
import io.grpc.ServerBuilder
import org.vindkaldr.note.grpc.Service
import org.vindkaldr.note.grpc.module.GrpcModule

const val DEFAULT_PORT = 50051

class GrpcService(private val grpcNoteService: GrpcNoteService) : Service {
    constructor(grpcModule: GrpcModule) : this(grpcModule.grpcNoteService)

    private lateinit var server: Server

    override fun start() {
        server = ServerBuilder.forPort(getPort())
            .addService(grpcNoteService)
            .build()
            .start()
    }

    private fun getPort(): Int {
        val portFromEnvironment  = System.getenv("PORT") ?: ""
        return when {
            portFromEnvironment.isNotBlank() -> portFromEnvironment.toInt()
            else -> DEFAULT_PORT
        }
    }

    override fun awaitTermination() {
        server.awaitTermination()
    }
}
