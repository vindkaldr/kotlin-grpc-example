package org.vindkaldr.note.grpc

interface Service {
    fun start()
    fun awaitTermination()
}
