package org.vindkaldr.note.grpc.module

import org.vindkaldr.note.GrpcNoteService
import org.vindkaldr.note.api.NoteBoundary
import org.vindkaldr.note.grpc.Service

interface GrpcModule {
    val service: Service
    val grpcNoteService: GrpcNoteService
    val noteBoundary: NoteBoundary
}
