package org.vindkaldr.note.main

fun main() {
    val service = MainModule().service
    service.start()
    service.awaitTermination()
}
