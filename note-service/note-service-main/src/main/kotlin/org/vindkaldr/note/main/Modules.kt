package org.vindkaldr.note.main

import org.vindkaldr.account.client.AccountServiceClientFactory
import org.vindkaldr.note.DefaultNoteBoundary
import org.vindkaldr.note.GrpcNoteService
import org.vindkaldr.note.GrpcService
import org.vindkaldr.note.NoteService
import org.vindkaldr.note.api.CreateNoteRequest
import org.vindkaldr.note.api.CreateNoteResponse
import org.vindkaldr.note.api.NoteBoundary
import org.vindkaldr.note.grpc.module.GrpcModule
import org.vindkaldr.note.module.NoteModule
import org.vindkaldr.note.module.PluginsModule
import org.vindkaldr.note.repository.InMemoryNoteRepository

class MainModule {
    private val noteModuleFactory: NoteModuleFactory =
        { correlationId, apiToken -> DefaultNoteModule(correlationId, apiToken, pluginsModule) }

    private val grpcModule by lazy { DefaultGrpcModule(noteModuleFactory) }
    private val pluginsModule by lazy { DefaultPluginsModule() }

    val service by lazy { grpcModule.service }
}

private class DefaultNoteModule(
    override val correlationId: String,
    private val apiToken: String,
    private val pluginsModule: PluginsModule
) : NoteModule {
    override val noteBoundary by lazy { DefaultNoteBoundary(this) }
    override val noteService by lazy { NoteService(this, pluginsModule) }
    override val registeredAccount by lazy {
        println("($correlationId) loading account")
        pluginsModule.accountServiceClient.readAccount(correlationId, apiToken)
    }
}

private typealias NoteModuleFactory = (correlationId: String, apiToken: String) -> NoteModule

private class DefaultGrpcModule(
    private val noteModuleFactory: NoteModuleFactory
) : GrpcModule {
    override val service by lazy { GrpcService(this) }
    override val grpcNoteService by lazy { GrpcNoteService(this) }
    override val noteBoundary by lazy {
        object : NoteBoundary {
            override fun createNote(request: CreateNoteRequest): CreateNoteResponse {
                val noteModule = noteModuleFactory(request.correlationId, request.apiToken)
                val noteBoundary = noteModule.noteBoundary
                return noteBoundary.createNote(request)
            }
        }
    }
}

private class DefaultPluginsModule : PluginsModule {
    override val accountServiceClient by lazy { AccountServiceClientFactory.create() }
    override val noteRepository by lazy { InMemoryNoteRepository() }
}
