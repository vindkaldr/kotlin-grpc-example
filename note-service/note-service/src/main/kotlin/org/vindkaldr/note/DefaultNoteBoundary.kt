package org.vindkaldr.note

import org.vindkaldr.note.api.CreateNoteRequest
import org.vindkaldr.note.api.CreateNoteResponse
import org.vindkaldr.note.api.NoteBoundary
import org.vindkaldr.note.module.NoteModule

class DefaultNoteBoundary(private val noteService: NoteService): NoteBoundary {
    constructor(noteModule: NoteModule) : this(noteModule.noteService)

    override fun createNote(request: CreateNoteRequest): CreateNoteResponse {
        return CreateNoteResponse(noteService.create(request.note))
    }
}
