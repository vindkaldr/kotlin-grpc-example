package org.vindkaldr.note

import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.note.api.Note
import org.vindkaldr.note.module.NoteModule
import org.vindkaldr.note.module.PluginsModule
import org.vindkaldr.note.repository.api.NoteRepository
import java.util.UUID

class NoteService(
    private val correlationId: String,
    private val account: RegisteredAccount,
    private val noteRepository: NoteRepository
) {
    constructor(noteModule: NoteModule, pluginsModule: PluginsModule) : this(
        noteModule.correlationId,
        noteModule.registeredAccount,
        pluginsModule.noteRepository
    )

    fun create(note: Note): Note {
        val noteWithNewId = note.copy(createId())
        noteRepository.add(noteWithNewId)
        println("($correlationId) Created note: apiToken: ${account.apiToken}, note: $note")

        return noteWithNewId
    }

    private fun createId() = UUID.randomUUID().toString()
}
