package org.vindkaldr.note.module

import org.vindkaldr.account.client.api.RegisteredAccount
import org.vindkaldr.note.NoteService
import org.vindkaldr.note.api.NoteBoundary

interface NoteModule {
    val noteBoundary: NoteBoundary
    val noteService: NoteService
    val registeredAccount: RegisteredAccount
    val correlationId: String
}
