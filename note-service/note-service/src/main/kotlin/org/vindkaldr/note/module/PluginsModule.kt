package org.vindkaldr.note.module

import org.vindkaldr.account.client.api.AccountServiceClient
import org.vindkaldr.note.repository.api.NoteRepository

interface PluginsModule {
    val accountServiceClient: AccountServiceClient
    val noteRepository: NoteRepository
}
