package org.vindkaldr.note.repository

import org.vindkaldr.note.api.Note
import org.vindkaldr.note.repository.api.NoteRepository

class InMemoryNoteRepository : NoteRepository {
    private val notes = mutableMapOf<String, Note>()

    override fun add(note: Note) {
        notes[note.id] = note
    }

    override fun contains(noteId: String): Boolean {
        return notes.containsKey(noteId)
    }

    override fun get(noteId: String): Note {
        return notes[noteId] ?: throw IllegalArgumentException()
    }

    override fun remove(noteId: String) {
        notes.remove(noteId)
    }
}
