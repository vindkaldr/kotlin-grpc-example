package org.vindkaldr.note.repository.api

import org.vindkaldr.note.api.Note

interface NoteRepository {
    fun add(note: Note)
    fun contains(noteId: String): Boolean
    fun get(noteId: String): Note
    fun remove(noteId: String)
}
