package org.vindkaldr.service

import org.vindkaldr.account.client.AccountServiceClientFactory
import org.vindkaldr.account.client.api.AccountServiceClient
import org.vindkaldr.note.api.Note
import org.vindkaldr.note.client.NoteServiceClientFactory
import org.vindkaldr.note.client.api.NoteServiceClient
import java.util.UUID
import kotlin.concurrent.thread

fun main() {
    val accountServiceClient = AccountServiceClientFactory.create()
    val noteServiceClient = NoteServiceClientFactory.create()

    repeat(1) {
        thread {
            while (true) {
                try {
                    val apiToken = createAccount(accountServiceClient)
                    readAccount(accountServiceClient, apiToken)
                    createNote(noteServiceClient, apiToken)
                    deleteAccount(accountServiceClient, apiToken)
                } catch (t: Throwable) {
                    println("Service unavailable: ${t.message}!")
                }
                Thread.sleep(3000)
            }
        }
    }
}

private fun createAccount(accountServiceClient: AccountServiceClient): String {
    val account = accountServiceClient.createAccount("vindkaldr@vindkaldr.org")
    println("Created account: apiToken: ${account.apiToken}, account: ${account.email}")
    return account.apiToken
}

private fun readAccount(accountServiceClient: AccountServiceClient, apiToken: String) {
    val account = accountServiceClient.readAccount(UUID.randomUUID().toString(), apiToken)
    println("Read account: apiToken: ${account.apiToken}, account: ${account.email}")
}

fun createNote(noteService: NoteServiceClient, apiToken: String) {
    val correlationId = UUID.randomUUID().toString()
    println("($correlationId) Creating note")
    val note = noteService.createNote(correlationId, apiToken, Note("", "an important note"))
    println("Created note: apiToken: $apiToken, note: ${note.id} ${note.note}")
    try {
        val impossibleCorrelationId = UUID.randomUUID().toString()
        println("($impossibleCorrelationId) Creating impossible note")
        noteService.createNote(impossibleCorrelationId, "${apiToken}asd", Note("", "impossible note"))
    }
    catch (t: Throwable) {
        println("Create note error: $t")
    }
}

private fun deleteAccount(accountServiceClient: AccountServiceClient, apiToken: String) {
    accountServiceClient.deleteAccount(apiToken)
    println("Deleted account: apiToken: $apiToken")
}
